
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var partials=require('express-partials');
//var userlist=require('./routes/userlist');
var http = require('http');
var path = require('path');
var settings=require('./settings');
var MongoStore=require('connect-mongo')(express);
var flash = require('connect-flash');
/*
var sessionStore=new MongoStore({
	db:settings.db
},function(){
	console.log('connect mongodb success...');
});
*/

var app = express();


// all environments
app.configure(function() {
	app.set('port', process.env.PORT || 3000);
	app.set('views', path.join(__dirname, 'views'));
	app.set('view engine', 'ejs');
	app.use(partials());
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.json());
	app.use(express.urlencoded());
	app.use(express.methodOverride());

	app.use(express.cookieParser());
	app.use(flash());
	app.use(express.session({
		secret:settings.cookieSecret,
		store:new MongoStore({
			db:settings.db,
			host:settings.host,
			port:settings.port,
			username:settings.username,
			password:settings.password
		})
	}));
	app.use(function(req, res, next){
	  var error = req.flash('error');
	  var success = req.flash('success');
	  res.locals.user = req.session.user;
	  res.locals.error = error.length ? error : null;
	  res.locals.success = success.length ? success : null;
	  next();
	});
	app.use(app.router);
	app.use(express.static(path.join(__dirname, 'public')));
});

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}





routes(app);
//app.get('/', routes.index);
//app.get('/users', user.list);
//app.get('/u/:user',routes.user);
//app.get('/post',routes.post);
//app.get('/login',routes.login_get);
///app.post('/reg',routes.reg);
//app.post('/login',route.login);
//app.get('/logout',routes.logout);
//app.get('/userlist', routes.userlist);
/*
app.dynamicHelpers({
	user:function(req,res){
		return req.session.user;
	},
	error:function(req,res){
		var err = req.flash('error');
		if(err.length){
			return err;
		}else{
			return null;
		}
	},
	success:function(req,res){
		var succ=req.flash('success');
		if(succ.length){
			return succ;
		}else{
			return null;
		}
	}
});*/
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
